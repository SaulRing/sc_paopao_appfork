pipeline {
  agent any

  environment{
    DOCKER_USER = 'saulring'
    DOCKER_HUB = credentials('docker_hub_pass')
    SONAR_TOKEN = credentials('sonar_token')
    EMAIL_TO = 'saulcaspamiranda@gmail.com'
  }
  
  stages {
    stage('build') {
      steps {
        sh 'chmod +x gradlew'
        sh './gradlew clean build -x test'
      }
      post {
        always {
          archiveArtifacts artifacts: 'build/libs/**/*.jar', fingerprint: true
        }
      }
    }

    stage('test') {
      steps {
        sh './gradlew check --info'
      }
      post {
        always {
          junit 'build/test-results/test/*.xml'
        }
      }
    }

    stage('code quality') {
        steps {
        sh './gradlew sonarqube'
      }
    }

    stage('package') {
      steps {
        sh 'docker build -t paopao-app:1.2 .'
      }
    }

    stage('publish') {
      steps {
        sh 'docker login -u "${DOCKER_HUB_USR}" -p "${DOCKER_HUB_PSW}"'
        sh 'docker tag paopao-app:1.2 saulring/paopao-app:1.2'
        sh 'docker push saulring/paopao-app:1.2'
      }
    }

    stage('deploy to dev') {
      steps {
        sh 'docker-compose up -d'
        sh 'yes | docker image prune -a'
      }
    }
  }

  post {
    always {
      emailext attachLog: true,
        attachmentsPattern: 'log.txt',
        subject: "Build #${env.BUILD_NUMBER} with Status: ${currentBuild.currentResult}",
        body: "Project: ${env.JOB_NAME} \n Build #: ${env.BUILD_NUMBER} \n Status: ${currentBuild.currentResult} \n Build URL: ${env.BUILD_URL} \n Commit: ${env.GIT_COMMIT}",
        to: "${EMAIL_TO}";
    }
  }
}